﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace warpictureBox
{
    public partial class Form1 : Form
    {
        public int soldierSpeed;
        public int archerSpeed;
        public int cheker;

        public int RedPoint;
        public int BluePoint;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            RedPoint = 0;
            BluePoint = 0;
            arrow1.Visible = false;
            arrow2.Visible = false;
            arrow3.Visible = false;
            shootTimer.Start();
            setSoldierMovement(sender, e);
            setArcherMovement(sender, e);
        }

        public void setSoldierMovement(object sender, EventArgs e)
        {
            ThreesecondDelay.Tick += new EventHandler(setSoldierMovement);
            ThreesecondDelay.Stop();
            troopAdvncetime.Tick += new EventHandler(MoveSolidierForwar);
            troopAdvncetime.Start();
        }
        public void setArcherMovement(object sender, EventArgs e)
        {
            archerAdvncTime.Tick += new EventHandler(MoveArcherForward);
            archerAdvncTime.Start();
        }
        public void MoveArcherForward(object sender, EventArgs e)
        {
            archerSpeed = -3;
            if (archerfront.Bounds.IntersectsWith(soldieBack.Bounds))
            {
                archerSpeed = 0;
            }
            if (archerfront.Bounds.IntersectsWith(archerLimit.Bounds))
            {
                archerAdvncTime.Stop();
            }
            archerfront.Left = archerfront.Left - archerSpeed;
            pictureBox105.Left = pictureBox105.Left - archerSpeed;
            pictureBox106.Left = pictureBox106.Left - archerSpeed;
            pictureBox107.Left = pictureBox107.Left - archerSpeed;
            pictureBox108.Left = pictureBox108.Left - archerSpeed;
            pictureBox104.Left = pictureBox104.Left - archerSpeed;

        }

        public void MoveSolidierForwar(object sender, EventArgs e)
        {
            soldierSpeed = -2;
            if (soldierFront.Bounds.IntersectsWith(redFrontsoldier.Bounds))
            {
                soldierSpeed = 0;
                SoldierAttackDelay.Tick += new EventHandler(callTroopPointSystem);
                SoldierAttackDelay.Start();
                troopAdvncetime.Stop();

            }
            soldieBack.Left = soldieBack.Left - soldierSpeed; 
            soldierFront.Left = soldierFront.Left - soldierSpeed;
            pictureBox56.Left = pictureBox56.Left - soldierSpeed;
            pictureBox57.Left = pictureBox57.Left - soldierSpeed;
            pictureBox58.Left = pictureBox58.Left - soldierSpeed;
            pictureBox59.Left = pictureBox59.Left - soldierSpeed;
            pictureBox60.Left = pictureBox60.Left - soldierSpeed;
            pictureBox61.Left = pictureBox61.Left - soldierSpeed;
            pictureBox62.Left = pictureBox62.Left - soldierSpeed;
            pictureBox63.Left = pictureBox63.Left - soldierSpeed;
            pictureBox64.Left = pictureBox64.Left - soldierSpeed;
            pictureBox65.Left = pictureBox65.Left - soldierSpeed;
            pictureBox66.Left = pictureBox66.Left - soldierSpeed;
            pictureBox67.Left = pictureBox67.Left - soldierSpeed;
            pictureBox68.Left = pictureBox68.Left - soldierSpeed;
            pictureBox69.Left = pictureBox69.Left - soldierSpeed;
            pictureBox70.Left = pictureBox70.Left - soldierSpeed;
            pictureBox71.Left = pictureBox71.Left - soldierSpeed;
            pictureBox72.Left = pictureBox72.Left - soldierSpeed;
            pictureBox73.Left = pictureBox73.Left - soldierSpeed;
            pictureBox74.Left = pictureBox74.Left - soldierSpeed;
            pictureBox75.Left = pictureBox75.Left - soldierSpeed;
            pictureBox76.Left = pictureBox76.Left - soldierSpeed;
            pictureBox77.Left = pictureBox77.Left - soldierSpeed;
            pictureBox78.Left = pictureBox78.Left - soldierSpeed;
            pictureBox79.Left = pictureBox79.Left - soldierSpeed;
            pictureBox80.Left = pictureBox80.Left - soldierSpeed;
            pictureBox81.Left = pictureBox81.Left - soldierSpeed;
            pictureBox82.Left = pictureBox82.Left - soldierSpeed;
            pictureBox83.Left = pictureBox83.Left - soldierSpeed;
            pictureBox84.Left = pictureBox84.Left - soldierSpeed;
            pictureBox85.Left = pictureBox85.Left - soldierSpeed;
            pictureBox86.Left = pictureBox86.Left - soldierSpeed;
            pictureBox87.Left = pictureBox87.Left - soldierSpeed;
            pictureBox88.Left = pictureBox88.Left - soldierSpeed;
            pictureBox89.Left = pictureBox89.Left - soldierSpeed;
            pictureBox90.Left = pictureBox90.Left - soldierSpeed;
            pictureBox91.Left = pictureBox91.Left - soldierSpeed;
            pictureBox92.Left = pictureBox92.Left - soldierSpeed;
            pictureBox93.Left = pictureBox93.Left - soldierSpeed;
            pictureBox94.Left = pictureBox94.Left - soldierSpeed;
            pictureBox95.Left = pictureBox95.Left - soldierSpeed;
            pictureBox96.Left = pictureBox96.Left - soldierSpeed;
            pictureBox97.Left = pictureBox97.Left - soldierSpeed;
            pictureBox98.Left = pictureBox98.Left - soldierSpeed;
            pictureBox99.Left = pictureBox99.Left - soldierSpeed;
            pictureBox100.Left = pictureBox100.Left - soldierSpeed;
            pictureBox101.Left = pictureBox101.Left - soldierSpeed;
            pictureBox102.Left = pictureBox102.Left - soldierSpeed;
            pictureBox103.Left = pictureBox103.Left - soldierSpeed;
            if(soldierFront.Bounds.IntersectsWith(wall1.Bounds))
            {
                wall1.Top = wall1.Top + 1000;
                troopAdvncetime.Stop();
                ThreesecondDelay.Start();
            }
            if (soldierFront.Bounds.IntersectsWith(wall2.Bounds))
            {
                wall2.Top = wall2.Top + 1000;
                troopAdvncetime.Stop();
                ThreesecondDelay.Start();
            }
            if (soldierFront.Bounds.IntersectsWith(wall3.Bounds))
            {
                wall3.Top = wall3.Top + 1000;
                troopAdvncetime.Stop();
                ThreesecondDelay.Start();
            }
            if (soldierFront.Bounds.IntersectsWith(wall3.Bounds))
            {   
                wall3.Top = wall1.Top + 1000;
                troopAdvncetime.Stop();
                ThreesecondDelay.Start();
            }
            if (soldierFront.Bounds.IntersectsWith(wall4.Bounds))
            {
                wall4.Top = wall1.Top + 1000;
                troopAdvncetime.Stop();
                ThreesecondDelay.Start();
            }
            if (soldierFront.Bounds.IntersectsWith(CWallfronttop.Bounds))
            {
                CWallfronttop.Top = CWallfronttop.Top + 1000;
                CWallfrontBottom.Top = CWallfrontBottom.Top + 1000;
                troopAdvncetime.Stop();
                ThreesecondDelay.Start();
            }

        }
        private void pictureBox9_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox52_Click(object sender, EventArgs e)
        {

        }

        private void troopAdvncetime_Tick(object sender, EventArgs e)
        {

        }

        private void shoot_Click(object sender, EventArgs e)
        {

            PointSystem(100, sender, e);
            arrow1.Visible = true;
            arrow2.Visible = true;
            arrow3.Visible = true;
            arrowSpeed.Tick += new EventHandler(moveArrow);
            arrowSpeed.Start();
        }
        public void moveArrow(object sender, EventArgs e)
        {
            if (pictureBox111.Bounds.IntersectsWith(arrowStop.Bounds))
            {
                arrow1.Visible = false;
                arrow2.Visible = false;
                arrow3.Visible = false;
                arrowSpeed.Stop();
                arrowSpeed.Tick += new EventHandler(moveArrowBack);
                arrowSpeed.Start();

            }
            pictureBox111.Left = pictureBox111.Left + 20;
            arrow1.Left = arrow1.Left + 20;
            arrow2.Left = arrow2.Left + 20;
            arrow3.Left = arrow3.Left + 20;
        }
        public void moveArrowBack(object sender, EventArgs e)
        {
            if (pictureBox112.Bounds.IntersectsWith(arrowStop.Bounds))
            {
                arrowSpeed.Stop();
            }
            pictureBox111.Left = pictureBox111.Left - 20;
            arrow1.Left = arrow1.Left - 20;
            arrow2.Left = arrow2.Left - 20;
            arrow3.Left = arrow3.Left - 20;
        }
        public void PointSystem(int Stakes, object sender, EventArgs e)
        {
            Random ran = new Random();
            int RnB = ran.Next(0, 2);
            if(RnB == 0)
            {
                RedPoint += Stakes;
                lblRedpoint.Text =""+ RedPoint;
            }
            else
            {
                BluePoint += Stakes;
                lblBluepoint.Text = "" + BluePoint;
            }
            if(RedPoint >=1000)
            {
                lblWiner.ForeColor = Color.Red;
                lblWiner.Text = "Red won";
            }
            else if (BluePoint >= 1000)
            {

                lblWiner.ForeColor = Color.Blue;
                lblWiner.Text = "Blue won";
            }
        }
        public void callTroopPointSystem(object sender, EventArgs e)
        {
            PointSystem(1, sender, e);
        }

        private void launch_Click(object sender, EventArgs e)
        {
            PointSystem(150, sender, e);
        }
    }
}
