# SODV2101-Wargame

Rapid Application Design course project repository.

-------------------------------------------------------------------------------------------------------------------------------------------------------

##### **Project Description**

This project is a simple battle simulation game. The focus of design will primarily be on the user-interface, rather than on gameplay itself. As such, gameplay will be fairly simple. 

The gameplay consists of two opposing armies, represented by simple pixelated shapes on the screen. There will be three troop types: foot soldiers, archers, and trebuchets. The opposing armies will move across the screen toward each other, one controlled by AI, one by player. The map will include some type of obstacles; When an army hits an obstacle, they cannot advance further for a short time until the obstacle is destroyed. When the armies collide, the program will automatically determine a winner and loser. The foot soldier that loses will disappear from the screen. 

There will be four buttons on-screen that may be controlled by the keyboard:

1. Halt - The army will halt for a short time. There will be a limited number of uses.
2. Retreat - The army will retreat so long as the button is held down for up to a time limit.
3. Archersí attack - The archers make a ranged attack. Can only be used every few seconds.
4. Trebuchetsí attack - The trebuchets make a ranged attack. Can only be used every few seconds, less frequently than archers.

The above will include some visual indications for the users to clearly understand when time limits have been reached or uses have been exhausted.


##### **Key Features**

1. Main Menu:
	* Start game
	* View high scores
	* Exit game
2. Button / keyboard commands to control the army.
3. Event-triggered sounds.
4. Random maps with variable on-screen obstacles.
5. Multiple troop types differentiated by simple graphics.
6. High-scores for sequential wins.

If time permits, the following may also be added:

1. Multiplayer 
	* Both armies controlled via keyboard, rather than buttons.
	* Option from the main screen.
2. More troop types
	* Unique mechanics.
	* Unique simple graphics style.
3. Obstacles that slow rather than stop movement.
4. Hand-made maps that may be chosen from the main menu.
