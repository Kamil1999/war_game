﻿namespace WarGame
{
    partial class ActionButtons
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnHalt = new System.Windows.Forms.Button();
            this.btnRetreat = new System.Windows.Forms.Button();
            this.btnArchers = new System.Windows.Forms.Button();
            this.btnTrebuchet = new System.Windows.Forms.Button();
            this.timerHalt = new System.Windows.Forms.Timer(this.components);
            this.timerRetreat = new System.Windows.Forms.Timer(this.components);
            this.timerArcher = new System.Windows.Forms.Timer(this.components);
            this.timerTrebuchet = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // btnHalt
            // 
            this.btnHalt.Enabled = false;
            this.btnHalt.Location = new System.Drawing.Point(3, 33);
            this.btnHalt.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnHalt.Name = "btnHalt";
            this.btnHalt.Size = new System.Drawing.Size(92, 53);
            this.btnHalt.TabIndex = 0;
            this.btnHalt.Text = "Halt";
            this.btnHalt.UseVisualStyleBackColor = true;
            this.btnHalt.Click += new System.EventHandler(this.Halt_Click);
            // 
            // btnRetreat
            // 
            this.btnRetreat.Enabled = false;
            this.btnRetreat.Location = new System.Drawing.Point(100, 33);
            this.btnRetreat.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRetreat.Name = "btnRetreat";
            this.btnRetreat.Size = new System.Drawing.Size(93, 55);
            this.btnRetreat.TabIndex = 1;
            this.btnRetreat.Text = "Retreat";
            this.btnRetreat.UseVisualStyleBackColor = true;
            this.btnRetreat.Click += new System.EventHandler(this.Retreat_Click);
            // 
            // btnArchers
            // 
            this.btnArchers.Location = new System.Drawing.Point(3, 94);
            this.btnArchers.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnArchers.Name = "btnArchers";
            this.btnArchers.Size = new System.Drawing.Size(92, 58);
            this.btnArchers.TabIndex = 2;
            this.btnArchers.Text = "Archers";
            this.btnArchers.UseVisualStyleBackColor = true;
            this.btnArchers.Click += new System.EventHandler(this.Archers_Click);
            // 
            // btnTrebuchet
            // 
            this.btnTrebuchet.Location = new System.Drawing.Point(100, 94);
            this.btnTrebuchet.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnTrebuchet.Name = "btnTrebuchet";
            this.btnTrebuchet.Size = new System.Drawing.Size(93, 58);
            this.btnTrebuchet.TabIndex = 3;
            this.btnTrebuchet.Text = "Trebuchet";
            this.btnTrebuchet.UseVisualStyleBackColor = true;
            this.btnTrebuchet.Click += new System.EventHandler(this.Trebuchet_Click);
            // 
            // timerHalt
            // 
            this.timerHalt.Interval = 1000;
            // 
            // timerRetreat
            // 
            this.timerRetreat.Interval = 1000;
            // 
            // timerArcher
            // 
            this.timerArcher.Interval = 1000;
            // 
            // timerTrebuchet
            // 
            this.timerTrebuchet.Interval = 1000;
            // 
            // ActionButtons
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnTrebuchet);
            this.Controls.Add(this.btnArchers);
            this.Controls.Add(this.btnRetreat);
            this.Controls.Add(this.btnHalt);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "ActionButtons";
            this.Size = new System.Drawing.Size(196, 197);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnHalt;
        private System.Windows.Forms.Button btnRetreat;
        private System.Windows.Forms.Button btnArchers;
        private System.Windows.Forms.Button btnTrebuchet;
        private System.Windows.Forms.Timer timerHalt;
        private System.Windows.Forms.Timer timerRetreat;
        private System.Windows.Forms.Timer timerArcher;
        private System.Windows.Forms.Timer timerTrebuchet;
    }
}
