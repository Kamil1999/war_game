﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WarGame
{
    public partial class ActionButtons : UserControl
    {
        public bool HaltIsPressed { get; set; } = false;
        public bool RetreatIsPressed { get; set; } = false;

        // Defiing get and set in this way allows the parent form (or other forms) to
        // access and modify the buttons directly.
        public bool HaltEnabled
        {
            get { return HaltEnabled; }
            set { btnHalt.Enabled = value; }
        }
        public bool RetreatEnabled
        {
            get { return RetreatEnabled; }
            set { btnRetreat.Enabled = value; }
        }
        public Timer button3sTimer = new Timer();
        public Timer button10sTimer = new Timer();



        public ActionButtons()
        {
            InitializeComponent();           
        }

        // 3 second timer.
        // Changes movement for 3s.
        private void button3sTimer_Tick(object sender, EventArgs e)
        {
            if (HaltIsPressed)
            {
                HaltIsPressed = false;
                if (!HaltEnabled) HaltEnabled = true;
            }
            if (RetreatIsPressed)
            {
                RetreatIsPressed = false;
                if (!RetreatEnabled) RetreatEnabled = true;
            }
        }




        public void Halt_Click(object sender, EventArgs e)
        {
            HaltIsPressed = HaltIsPressed ? false : true;
        }

        public void Retreat_Click(object sender, EventArgs e)
        {
            RetreatIsPressed =  RetreatIsPressed ? false : true;
            
        }

        public void Trebuchet_Click(object sender, EventArgs e)
        {

        }

        public void Archers_Click(object sender, EventArgs e)
        {

        }
    }
}
