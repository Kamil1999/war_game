﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using System.Windows.Forms;

namespace WarGame
{
    class GamePlay
    {
        public List<Scores> TopScores = new List<Scores>();
        public int WinCount;
        public bool DidWin = false;

        public void CreateFootSoldiers(object sender, PaintEventArgs e, int x, int y, string color, int counter)
        {
            int _x = x;
            int _y = y;
            var _color = Brushes.Black;

            if(color == "Red")
            {
               _color = Brushes.Red;
            }
            else
            {
               _color = Brushes.Blue;
            }

            for (int i = 0; i < counter; i++)
            {
                e.Graphics.FillRectangle(_color, _x, _y, 15, 15);
                e.Graphics.FillRectangle(_color, _x + 20, _y, 15, 15);
                e.Graphics.FillRectangle(_color, _x + 40, _y, 15, 15);

                e.Graphics.FillRectangle(_color, _x, _y + 20, 15, 15);
                e.Graphics.FillRectangle(_color, _x + 20, _y + 20, 15, 15);
                e.Graphics.FillRectangle(_color, _x + 40, _y + 20, 15, 15);

                e.Graphics.FillRectangle(_color, _x, _y + 40, 15, 15);
                e.Graphics.FillRectangle(_color, _x + 20, _y + 40, 15, 15);
                e.Graphics.FillRectangle(_color, _x + 40, _y + 40, 15, 15);

                if (i == 2)
                {
                    _x += 65;
                    _y -= 195;
                }
                _y += 65;
            }
        }
        public void CreateArcher(object sender, PaintEventArgs e, int x, int y, string color, int counter)
        {
            Graphics gr = default(Graphics);
            var _color = new Pen(Color.Black, 5);
            if (color == "Red")
            {
                _color = new Pen(Color.Red, 5);
            }
            else
            {
                _color = new Pen(Color.Blue, 5);
            }

            gr = e.Graphics;
            Point[] pnt = new Point[4];
            for (int i = 0; i < counter; i++)
            {
                pnt[0].X = 10 + x;
                pnt[0].Y = 0 + y;

                pnt[1].X = 20 + x;
                pnt[1].Y = 20 + y;

                pnt[2].X = 0 + x;
                pnt[2].Y = 20 + y;

                pnt[3].X = 10 + x;
                pnt[3].Y = 0 + y;

                gr.DrawPolygon(_color, pnt);
                y += 40;
            }
        }
        public void CreateTrebuchet(object sender, PaintEventArgs e, int x, int y, string color, int counter)
        {
            var _color = new Pen(Color.Black, 5);
            if (color == "Red")
            {
                _color = new Pen(Color.Red, 5);
            }
            else
            {
                _color = new Pen(Color.Blue, 5);
            }
            for (int i = 0; i < counter; i++)
            {
                e.Graphics.DrawEllipse(_color, x, y, 40, 40);
                y += 250;
            }
        }
        
        public void Play()
        {
            GetWins();
        }

        // Load the number of consecutive wins, save it to WinCount.
        public void GetWins()
        {
            // File will be created if it doesn't pre-exist
            using (StreamWriter sw = new StreamWriter("./wins.txt"))
            {
                sw.Write(WinCount);
            }

            //Read the context of the.txt, print it in the game interface.
            using (StreamReader sr = new StreamReader("./wins.txt"))
            {
                WinCount = Convert.ToInt32(sr.ReadLine());
            }
        }

        public bool WinnerCheck()
        {
            // TODO
            return false; // TODO change or remove this as needed. 
        }

        public void EndGame(bool didWin)
        {
            // if the player wins, add one to their win count.
            WinCount = didWin ? WinCount + 1 : 0;

            // TODO
        }

        //public void SaveWins(Human player)
        //{
        //    // TODO:
        //    // Create List<int> TopScores (or should it be a class with string playername and int
        //    // score?). ✔
        //    // Access the Human.cs score. ✔
        //    // If the new score is higher than any in the list or if there are fewer than 10,
        //    // add the new score above the lower score or into the opening.
        //    // Read / write TopScores list to .txt file

        //    foreach (Scores item in TopScores)
        //    {
        //        if (player.Score > item.Score)
        //        {
        //            // Shift all items in list down.
        //            // Create, add new Score to list.
        //        }
                    
        //    }

        //    // Add name and score then new line to "Scores.txt"
        //    File.AppendAllText("Scores.txt", player.Name + "\t\t\t\t\t" + player.Score + Environment.NewLine);
        //}

    }
}
