﻿namespace WarGame
{
    partial class High_Scores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBoxScores = new System.Windows.Forms.RichTextBox();
            this.lblScoresTitle = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // richTextBoxScores
            // 
            this.richTextBoxScores.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBoxScores.DetectUrls = false;
            this.richTextBoxScores.Enabled = false;
            this.richTextBoxScores.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBoxScores.Location = new System.Drawing.Point(12, 62);
            this.richTextBoxScores.Name = "richTextBoxScores";
            this.richTextBoxScores.ReadOnly = true;
            this.richTextBoxScores.Size = new System.Drawing.Size(434, 515);
            this.richTextBoxScores.TabIndex = 0;
            this.richTextBoxScores.Text = "";
            this.richTextBoxScores.TextChanged += new System.EventHandler(this.RichTextBoxScores_TextChanged);
            // 
            // lblScoresTitle
            // 
            this.lblScoresTitle.AutoSize = true;
            this.lblScoresTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblScoresTitle.Location = new System.Drawing.Point(148, 9);
            this.lblScoresTitle.Name = "lblScoresTitle";
            this.lblScoresTitle.Size = new System.Drawing.Size(151, 29);
            this.lblScoresTitle.TabIndex = 1;
            this.lblScoresTitle.Text = "High Scores";
            // 
            // High_Scores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(458, 589);
            this.Controls.Add(this.lblScoresTitle);
            this.Controls.Add(this.richTextBoxScores);
            this.Name = "High_Scores";
            this.Text = "High_Scores";
            this.Load += new System.EventHandler(this.High_Scores_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBoxScores;
        private System.Windows.Forms.Label lblScoresTitle;
    }
}