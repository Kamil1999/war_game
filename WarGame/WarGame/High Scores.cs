﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WarGame
{
    public partial class High_Scores : Form
    {
        public High_Scores()
        {
            InitializeComponent();
        }

        private void RichTextBoxScores_TextChanged(object sender, EventArgs e)
        {

        }

        private void High_Scores_Load(object sender, EventArgs e)
        {
            // TODO remove
            // Creates a temporary gamePlay instance to test saving/loading scores.
            GamePlay gp = new GamePlay();
            //Human hue = new Human(1, "Jon");
            //gp.SaveWins(hue);


            string line;

            using (StreamReader sr = new StreamReader("./Scores.txt"))
            {
                while ((line = sr.ReadLine()) != null)
                {
                    richTextBoxScores.Text += sr.ReadLine();
                    richTextBoxScores.Text += "\n";
                }
                
            }
        }
    }
}
