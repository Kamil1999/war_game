﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WarGame
{
    public partial class Form1 : Form
    {
        System.Media.SoundPlayer player = new System.Media.SoundPlayer();
        Timer buttonCooldownTimer = new Timer();

        public int SnumRed = 6;
        public int SnumBlue = 6;
        public int SArecherRed = 5;
        public int SArecherBlue = 5;
        public int STrebushetRed = 2;
        public int STrebushetBlue = 2;
        public bool nextBlock;
        public bool StartTimer = true;

        public int Smove;
        public int Amove;
        public double marker;
        public int blocksPrintNumber;
        public List<int> blockXcoordinate = new List<int> { 100, 180, 260, 340, 420 };

        public Form1()
        {
            InitializeComponent();
            player.SoundLocation = "sound.wav";
            Smove = 0;
            Amove = 0;
            blocksPrintNumber = 5;
            marker = 0;
            nextBlock = false;
            timer1.Start();

            // New Timer for actionButtons button cooldown.
            // Through God, all things are possible.
           
            buttonCooldownTimer.Tick += new EventHandler(buttonCooldownTimer_Tick);
            buttonCooldownTimer.Interval = 3000;
            buttonCooldownTimer.Start();
        }

        public void Form1_Load(object sender, EventArgs e)
        {
            //player.Play();
        }

        // After the first 3s of playing, the butons become enabled.
        private void buttonCooldownTimer_Tick(object sender, EventArgs e)
        {
            actionButtons1.HaltEnabled = true;
            actionButtons1.RetreatEnabled = true;
            buttonCooldownTimer.Stop();

        }
        private void buttonUseTimer_Tick(object sender, EventArgs e)
        {
            if (actionButtons1.HaltIsPressed) actionButtons1.HaltEnabled = false;
            if (actionButtons1.HaltIsPressed) actionButtons1.RetreatEnabled = false;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {


            // If 'Halt' button is pressed, stop their movement.
            // If 'Retreat' is pressed, reverse their movement.
            // Else, move forward (default).
            if (actionButtons1.HaltIsPressed)
            {
                Smove += 0;
                Amove += 0;
            }
            else if (actionButtons1.RetreatIsPressed)
            {
                Smove -= 1;
                Amove -= 1;
            }
            else
            {
                Smove += 2;
                Amove += 1;
            }

            //Look for contact and not print the toched block


            //Region region1 = new Region(new Rectangle(90 + Smove, 205, 138,185));
            Invalidate();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Timer Blockdestruction = new Timer();
            Blockdestruction.Tick += new EventHandler(BlockBool);
            Blockdestruction.Interval = 4000;
            if (Smove == blockXcoordinate[0] && nextBlock == false)
            {
                Smove -= 2;

                if (StartTimer)
                {
                    StartTimer = false;
                    Blockdestruction.Start();
                }

            }
            else if (StartTimer)
            {
                StartTimer = true;
                Blockdestruction.Stop();
            }
            //Drwas the map
            Maper makemap = new Maper(sender, e);
            makemap.Map(sender, e);
            makemap.Bloock(sender, e, blocksPrintNumber);

            // Draws the red soldiers.
            GamePlay RedTroops = new GamePlay();
            RedTroops.CreateFootSoldiers(sender, e, 710, 205, "Red", SnumRed);
            RedTroops.CreateArcher(sender, e, 840, 210, "Red", SArecherRed);
            RedTroops.CreateTrebuchet(sender, e, 830, 160, "Red", STrebushetRed);

            // Draws the blue soldiers.
            GamePlay BlueTroops = new GamePlay();
            BlueTroops.CreateFootSoldiers(sender, e, 100 + Smove, 205, "Blue", SnumBlue);
            BlueTroops.CreateArcher(sender, e, 40 + Amove, 210, "Blue", SArecherBlue);
            BlueTroops.CreateTrebuchet(sender, e, 30, 160, "Blue", STrebushetBlue);
        }

        private void BlockBool(object sender, EventArgs e)
        {
            nextBlock = false;
            StartTimer = true;
            Random r = new Random();
            int num = r.Next(0, 2);
            if (num == 1)
            {
                SnumRed--;
            }
            else
                SnumBlue--;

            int archer = r.Next(0, 3);
            if (archer == 0)
            {
                SArecherRed--;
            }
            else if (archer == 1)
                SArecherBlue--;

            int trebu = r.Next(0, 4);
            if (trebu == 0)
            {
                STrebushetRed--;
            }
            else if (trebu == 1)
                STrebushetBlue--;


            blocksPrintNumber--;
            if (blockXcoordinate.Count > 1)
                blockXcoordinate.RemoveAt(0);
        }
        private void actionButtons1_Load(object sender, EventArgs e)
        {
            
        }

        private void ActionButtons1_Load_1(object sender, EventArgs e)
        {

        }
    }
}
