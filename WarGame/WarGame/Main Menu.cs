﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WarGame
{
    public partial class Main_Menu : Form
    {
        public Main_Menu()
        {
            InitializeComponent();
        }

        private void BtnPlay_Click(object sender, EventArgs e)
        {
            Form1 playGame = new Form1();
            this.Hide(); // Must be in the Hide(), ShowDialog(), Show() order.
            playGame.ShowDialog(this);
            this.Show();

        }

        private void BtnScores_Click(object sender, EventArgs e)
        {
            High_Scores hs = new High_Scores();
            hs.ShowDialog();
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void TextBoxPlayerName_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void Main_Menu_Load(object sender, EventArgs e)
        {

        }
    }
}
