﻿using System;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WarGame
{
    public class Map : Form
    {

        public int X1 { get; set; }
        public int Y1 { get; set; }
        public int X2 { get; set; }
        public int Y2 { get; set; }
        //implement with differnt types of block
        //public string Name { get; set; }

        public Map(int x1, int y1, int x2, int y2, string name)
        {
            X1 = x1;
            Y1 = y1;
            X2 = x2;
            Y2 = y2;
            //Name = name;
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // Map
            // 
            this.ClientSize = new System.Drawing.Size(282, 253);
            this.Name = "Map";
            this.Load += new System.EventHandler(this.Map_Load);
            this.ResumeLayout(false);

        }

        private void Map_Load(object sender, EventArgs e)
        {

        }
    }
    public class holder
    {
        public List<Map> ArrBlockLocation { get; set; } = new List<Map>();
    }
    public class Maper:holder
    {
        public Maper(object sender, PaintEventArgs e)
        {
            Console.WriteLine(ArrBlockLocation.Count);
        }

        public void Map(object sender, PaintEventArgs e)
        {
            e.Graphics.FillRectangle(Brushes.Brown, 700, 150, 175, 5);
            e.Graphics.FillRectangle(Brushes.Brown, 700, 450, 175, 5);

            e.Graphics.FillRectangle(Brushes.Brown, 650, 150, 5, 305);

            e.Graphics.FillRectangle(Brushes.Brown, 700, 150, 5, 100);
            e.Graphics.FillRectangle(Brushes.Brown, 700, 350, 5, 100);

            e.Graphics.FillRectangle(Brushes.Brown, 875, 150, 5, 305);


        }
        public void Bloock(object sender, PaintEventArgs e, int blocks)
        {
            Random ran = new Random();
            Console.WriteLine(ArrBlockLocation.Count);

            for (int i = blocks; i > 0; i--)
            {
                bool ToSkip = false;
                int x = 700-(i*80);
                int y = 200;
                int W = 5;
                int H = 200;

                //Too close check
                if (!ToSkip)
                {
                    e.Graphics.FillRectangle(Brushes.Brown, x, y, W, H);

                    Map block = new Map(x, y, x + W, y + H, "Block" + i);

                    ArrBlockLocation.Add(block);
                }

            }
        }
        public void redrwa(object sender, PaintEventArgs e)
        {
            foreach (Map element in ArrBlockLocation)
            {
                e.Graphics.FillRectangle(Brushes.Brown, element.X1, element.Y1, element.X2 - element.X1, element.Y2 - element.Y1);
            }
        }
        

    }
}