﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarGame
{
    public abstract class Player
    {
        public int ID { get; set; }
        public string Name { get; set; }


        public Player(int id, string name)
        {
            this.ID = id;
            this.Name = name;
        }

        // We may want to change these to abstract.
        public virtual void HaltTroops()
        {
            // TODO 
        }

        public virtual void Retreat()
        {
            // TODO
        }

        public virtual void ArchersLoose()
        {
            // TODO
        }

        public virtual void TrebuchetsLoose()
        {
            // TODO
        }
    }
}
