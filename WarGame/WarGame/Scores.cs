﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarGame
{
    class Scores
    {
        public int Score { get; set; }
        public string Name { get; set; }

        Scores(int score, string playerName)
        {
            this.Name = playerName;
            this.Score = score;
        }
    }
}
